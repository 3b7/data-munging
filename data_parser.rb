require 'byebug'

class DataParser
  # @param filename [String]
  # @param elements [Array]
  def initialize(filename, elements)
    input = File.open(filename, File::RDONLY){ |f| f.read }
    @lines = input.lines.map(&:split)
    @elements = elements
  end

  attr_reader :lines, :elements

  def process
    lines = @lines.drop(1) # headers
    lines.each do |line|
      next if line.empty?
      puts "#{line[elements[0]]}: spread #{line[elements[1]].to_i - line[elements[-1]].to_i}"
    end
  end
end

DataParser.new('./weather.dat', [0,2,1]).process
DataParser.new('./football.dat',[1,6,8]).process
