require './data_parser'

describe DataParser do
  subject(:parser) { described_class.new('./football.dat', [1,6,8]) }
  let(:football_output) do
    "Arsenal: spread 43\nLiverpool: spread 37\nManchester_U: spread 42\nNewcastle: spread 22\n"\
    "Leeds: spread 16\nChelsea: spread 28\nWest_Ham: spread -9\nAston_Villa: spread -1\n"\
    "Tottenham: spread -4\nBlackburn: spread 4\nSouthampton: spread -8\n"\
    "Middlesbrough: spread -12\nFulham: spread -8\nCharlton: spread -11\nEverton: spread -12\n"\
    "Bolton: spread -18\nSunderland: spread -22\n: spread 0\nIpswich: spread -23\n"\
    "Derby: spread -30\nLeicester: spread -34\n"
  end

  describe '#initialize' do
    it 'initiates an instance' do
      expect(subject).to be_a(described_class)
    end
  end

  describe '#process' do
    it 'returns a string' do
      expect { subject.process }.to output(football_output).to_stdout
    end
  end
end
